import axios from '../../api/axios'

const requests = {
    // 查询功能管理控制器
    PostQueryData () {
        return axios('/function/query-tree', {
            method: 'post'
        })
    },
    // 添加功能管理控制器
    PostAddData (data) {
        return axios('/function/add', {
            method: 'post',
            data: data
        })
    },
    // 删除功能管理控制器
    PostDelData (data) {
        return axios('/function/del', {
            method: 'post',
            data: data
        })
    },
    // 修改功能管理控制器
    PostModifyData (data) {
        return axios('/function/modify', {
            method: 'post',
            data: data
        })
    }
}
export default requests