import axios from '../../api/axios'

const requests = {
    // 查询功能信息
    Information (data) {
        return axios('/function/query-tree', {
            method: 'post',
            data:data
        })
    },

    // 角色授权接口
    Roleauthorization (data,rid) {
        return axios('/function/rel-role-function?rid='+rid,{
            method: 'post',
            data:data
        })
    },

    // 查询角色已经授权的功能
    Roleauthor(rid) {
        return axios('/function/query-role-function/'+rid,{
            method: 'get',
        })
    }
}

export default requests