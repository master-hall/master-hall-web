import axios from '../../api/axios'
const require ={
    roleAdd(data){
        return axios('/post-role/add',{
            method: 'post',
            data : data,
        })
    },
    roleDel(data){
        return axios('/post-role/del',{
            method : 'post',
            data :data,
        })
    },
    roleModify(data){
        return axios('/post-role/modify',{
            method:'post',
            data:data,
        })
    },
    roleQuery(data) {
        return axios('/post-role/query',{
            method: 'post',
            data:data,
        })
    },
    roleUser(data) {
        return axios('/user-role/getUserId?rid='+data,{
            method:'post',
            data:data
        })
    },
    roleSave(data,rid) {
        return axios('/user-role/saveUserId?rid='+rid,{
            method: 'post',
            data:data,
        })
    },
    roleList(data) {
        return axios('/system-user/query',{
            method:'post',
            data:data
        })
    }
}
export default require
